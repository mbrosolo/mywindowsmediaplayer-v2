﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Forms;
using System.Windows.Threading;
using System.Windows.Input;
using System.Collections.Generic;
using System.IO;
using System.Windows.Data;

namespace MyWindowsMediaPlayerV2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DispatcherTimer timer;
        private bool _playing = false;
        public bool IsPlaying
        {
            set { _playing = value; btnPlay.IsChecked = value; }
            get { return _playing; }
        }
        private bool dragging = false;
        private bool fullScreen = false;
        private bool playlist = false;
        private bool _mute = false;
        public bool Mute
        {
            get { return _mute; }
            set { _mute = value; }
        }

        public MainWindow()
        {
            InitializeComponent();
            this.timer = new DispatcherTimer();
            this.timer.Interval = TimeSpan.FromSeconds(1);
            this.timer.Tick += new EventHandler(timer_Tick);
            this.MediaGrid.Background = Brushes.Black;
            this.lib.Visibility = Visibility.Collapsed;
            this.CurrentTime.Text = "0 : 0";
            this.TotalTime.Text = "0 : 0";
        }

        void Element_MediaStart(object sender, EventArgs e)
        {
            if (myMediaElement.NaturalDuration.HasTimeSpan)
            {
                TimeSpan ts = myMediaElement.NaturalDuration.TimeSpan;
                this.timelineSlider.Maximum = ts.TotalSeconds;
                this.timelineSlider.SmallChange = 1;
                this.timelineSlider.LargeChange = Math.Min(10, ts.Seconds / 10);

                this.CurrentTime.Text = "0 : 0";
                this.TotalTime.Text = ts.Minutes.ToString() + " : " + ts.Seconds.ToString();
            }
            this.timelineSlider.Value = 0;
            this.timer.Start();
        }

        void OnMouseDownPlayMedia(object sender, RoutedEventArgs e)
        {
            if (this.lib.Player.Current == null)
            {
                this.menuLoad(null, null);
                return;
            }
            if (IsPlaying == true)
            {
                myMediaElement.Pause();
                IsPlaying = false;
                timer.Stop();
            }
            else
            {
                myMediaElement.Play();
                IsPlaying = true;
                timer.Start();
            }
        }

        void OnMouseDownStopMedia(object sender, RoutedEventArgs e)
        {
            myMediaElement.Stop();
            IsPlaying = false;
            timer.Stop();
            timelineSlider.Value = 0;
            this.CurrentTime.Text = "0 : 0";
        }

        private void ChangeMediaVolume(object sender, RoutedPropertyChangedEventArgs<double> args)
        {
            myMediaElement.Volume = (double)volumeSlider.Value;
        }

        private void Element_MediaEnded(object sender, EventArgs e)
        {
            myMediaElement.Stop();
            IsPlaying = false;
            timer.Stop();
            timelineSlider.Value = 0;
            this.lib.Player.NextMedia();
            playMedia(this.lib.Player.Current);
        }

        private void Element_MediaFailed(object sender, EventArgs e)
        {
            Console.WriteLine("Loading Failed");
            IsPlaying = false;
            this.lib.Player.Current = null;
            this.lib.Player.NextMedia();
            playMedia(this.lib.Player.Current);
        }

        public void playMedia(Media media)
        {
            if (media == null)
            {
                IsPlaying = false;
                return;
            }

            myMediaElement.Source = new Uri(media.Path);
            try
            {
                myMediaElement.Play();
                IsPlaying = true;
                this.lib.Player.Current = media;


                this.lib.LibraryView.Visibility = Visibility.Visible;
                this.lib.currentPlaylist.Visibility = Visibility.Collapsed;
                this.lib.Visibility = Visibility.Collapsed;
                myMediaElement.Visibility = Visibility.Visible;
                playlist = false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                IsPlaying = false;
                this.lib.Player.Current = null;
            }
        }

        void menuLoadPlaylist(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog FDG = new System.Windows.Forms.OpenFileDialog();
            FDG.DefaultExt = ".xml";
            FDG.Filter = "Text documents (.xml)|*.xml";

            DialogResult result = FDG.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                PlayList m = new PlayList(FDG.FileName);
                this.lib.Current = m;
                this.lib.Player = new PlayListPlayer(this.lib.Current);
                this.lib.currentPlaylist.ItemsSource = this.lib.Player.Items;
                this.lib.currentPlaylist.Items.Refresh();
            }
            else
            {
                IsPlaying = false;
            }
        }

        void menuLoad(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog FDG = new System.Windows.Forms.OpenFileDialog();

            DialogResult result = FDG.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                Media m = new Media(FDG.FileName);
                lib.Player.AddPlayListItem(m);
                lib.currentPlaylist.Items.Refresh();
                playMedia(m);
            }
            else
            {
                IsPlaying = false;
            }
        }

        void addToLibrary(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog FDG = new System.Windows.Forms.OpenFileDialog();

            DialogResult result = FDG.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                this.lib.MyLibrary.AddMedia(FDG.FileName);
                lib.LibraryMedias.Items.Refresh();
            }
        }

        void menuExit(object sender, EventArgs e)
        {
            this.Close();
        }

        public void timer_Tick(object sender, EventArgs e)
        {
            if (!dragging)
                timelineSlider.Value = myMediaElement.Position.TotalSeconds;
            TimeSpan ts = TimeSpan.FromSeconds(timelineSlider.Value);

            this.CurrentTime.Text = ts.Minutes.ToString() + " : " + ts.Seconds.ToString();
        }

        private void timelineSlider_DragStarted(object sender, System.Windows.Controls.Primitives.DragStartedEventArgs e)
        {
            dragging = true;
        }

        private void timelineSlider_DragCompleted(object sender, System.Windows.Controls.Primitives.DragCompletedEventArgs e)
        {
            dragging = false;
            myMediaElement.Position = TimeSpan.FromSeconds(timelineSlider.Value);
        }

        private void timelineSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (!dragging)
                myMediaElement.Position = TimeSpan.FromSeconds(timelineSlider.Value);
        }

        private void Element_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                if (!fullScreen)
                {
                    var pos = myMediaElement.Position;
                    LayoutRoot.Children.Remove(MediaGrid);
                    this.Content = MediaGrid;
                    this.WindowStyle = WindowStyle.None;
                    this.WindowState = WindowState.Maximized;
                    myMediaElement.Position = pos;
                }
                else
                {
                    var pos = myMediaElement.Position;
                    this.Content = LayoutRoot;
                    LayoutRoot.Children.Add(MediaGrid);
                    this.WindowStyle = WindowStyle.SingleBorderWindow;
                    this.WindowState = WindowState.Normal;
                    myMediaElement.Position = pos;
                }
                fullScreen = !fullScreen;
            }
        }

        private void btnPlaylist_click(object sender, RoutedEventArgs e)
        {
            if (!playlist)
            {
                myMediaElement.Visibility = Visibility.Collapsed;
                this.lib.Visibility = Visibility.Visible;
                this.lib.LibraryView.Visibility = Visibility.Collapsed;
                this.lib.currentPlaylist.Visibility = Visibility.Visible;
                playlist = true;
            }
            else
            {
                this.lib.LibraryView.Visibility = Visibility.Visible;
                this.lib.currentPlaylist.Visibility = Visibility.Collapsed;
                this.lib.Visibility = Visibility.Collapsed;
                myMediaElement.Visibility = Visibility.Visible;
                playlist = false;
            }
        }

        private void playlistSave(object sender, EventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.FileName = this.lib.Current.FileName;
            dlg.DefaultExt = ".xml";
            dlg.Filter = "Text documents (.xml)|*.xml";

            string dir = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            dir += "\\MyPlaylists";
            if (Directory.Exists(dir) == false)
            {
                Directory.CreateDirectory(dir);
            }
            dlg.InitialDirectory = dir;

            // Show save file dialog box
            DialogResult result = dlg.ShowDialog();
            Console.WriteLine("stesttt : " + dlg.FileName);

            // Process save file dialog box results
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                this.lib.Player.Save();
                PlayList copy = new PlayList(this.lib.Current);
                copy.FileName = dlg.FileName;
                copy.Title = Path.GetFileName(dlg.FileName).Replace(".xml", "");
                copy.Serialize();
                this.lib.MyLibrary.AddPlayList(copy);
                Console.WriteLine("saving playlist : " + copy.Title);
                this.lib.LibraryPlaylists.Items.Refresh();
            }
        }

        private void MuteButton_Click(object sender, RoutedEventArgs e)
        {
            this.myMediaElement.IsMuted = !this.myMediaElement.IsMuted;
        }

        private void PrevButton_Click(object sender, RoutedEventArgs e)
        {
            this.lib.Player.Prev();
            this.playMedia(this.lib.Player.Current);
        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            this.lib.Player.Next();
            this.playMedia(this.lib.Player.Current);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.XPath;
using System.Xml;

namespace MyWindowsMediaPlayerV2
{
    public class CLibrary
    {
        private List<PlayList> _playlists;

        public List<PlayList> Playlists
        {
            get { return _playlists; }
        }

        private List<Media> _medias;

        public List<Media> Medias
        {
            get { return _medias; }
        }

        public string Filename { get; set; }

        public CLibrary(string filename) {
            this.Filename = filename;
            this._medias = new List<Media>();
            this._playlists = new List<PlayList>();
            try
            {
                XPathDocument doc = new XPathDocument(filename);
                XPathNavigator nav = doc.CreateNavigator();

                XPathNodeIterator iter = null;

                iter = nav.Select("Root/PlayLists/item");

                foreach (XPathNavigator item in iter)
                {
                    string cfilename = item.SelectSingleNode("filename").Value;
                    PlayList newplaylist = new PlayList(cfilename);
                    Console.WriteLine("Added new playlist : " + cfilename);
                    this._playlists.Add(newplaylist);
                }

                iter = nav.Select("Root/Files/item");

                foreach (XPathNavigator item in iter)
                {
                    string cfilename = item.SelectSingleNode("filename").Value;
                    Media  newmedia = new Media(cfilename);
                    Console.WriteLine("Added new media : " + cfilename);
                    this._medias.Add(newmedia);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void     AddMedia(string filename) {
            this._medias.Add(new Media(filename));
            this.Serialize();
        }

        public void AddMediaDirectory(string dirname) {
            this.Serialize();
        }

        public void AddPlayList(PlayList pl) {
            foreach (PlayList pl1 in this.Playlists) {
                if (pl1.Title == pl.Title) {
                    pl1.Items = pl.Items;
                    return;
                }
            }
            this._playlists.Add(pl);
            this.Serialize();
        }

        public PlayList CreatePlayList(string filename) {
            PlayList p = new PlayList(filename);

            this._playlists.Add(p);

            this.Serialize();
            return p;
        }

        public void RemoveMedia(Media m)
        {
            this._medias.Remove(m);
            this.Serialize();
        }

        public void RemovePlayList(PlayList pl) {
            this._playlists.Remove(pl);
            this.Serialize();
        }

        public void Serialize() {
            this.Serialize(this.Filename);
        }


        public void Serialize(string filename) { 
            XmlDocument doc = new XmlDocument();

            XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", null, null);
            doc.AppendChild(dec);

            doc.AppendChild(doc.CreateElement("Root"));

            XmlElement tmp = null;

            XmlElement files = doc.CreateElement("Files");
            doc.DocumentElement.AppendChild(files);

            foreach (Media item in this._medias)
            {
                XmlElement tmpnode = doc.CreateElement("item");
                tmp = doc.CreateElement("filename");
                tmp.InnerText = item.Path;
                tmpnode.AppendChild(tmp);
                files.AppendChild(tmpnode);
            }

            XmlElement playlists = doc.CreateElement("PlayLists");
            doc.DocumentElement.AppendChild(playlists);

            foreach (PlayList tmpplaylist in this._playlists)
            {
                XmlElement tmpnode = doc.CreateElement("item");
                tmp = doc.CreateElement("filename");
                tmp.InnerText = tmpplaylist.FileName;
                tmpnode.AppendChild(tmp);
                playlists.AppendChild(tmpnode);
            }

            System.IO.File.WriteAllText(filename, string.Empty);
            doc.Save(filename);
        }
    }
}

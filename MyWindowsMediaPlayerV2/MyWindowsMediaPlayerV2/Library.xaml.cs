﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace MyWindowsMediaPlayerV2
{
    /// <summary>
    /// Interaction logic for Library.xaml
    /// </summary>
    public partial class Library : UserControl
    {
        private CLibrary _library;
        public CLibrary MyLibrary
        {
            get { return _library; }
            set { _library = value; }
        }

        private PlayList _current;
        public PlayList Current
        {
            get { return _current; }
            set { _current = value; }
        }

        private PlayListPlayer _player;

        public PlayListPlayer Player
        {
            get { return _player; }
            set { _player = value; }
        }

        public Library()
        {
            InitializeComponent();
            this.Current = new PlayList("");

            string dir = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            dir += "\\MyPlaylists";
            if (Directory.Exists(dir) == false)
            {
                Directory.CreateDirectory(dir);
            }

            this.MyLibrary = new CLibrary(dir+"\\library.xml");
            this.currentPlaylist.Visibility = Visibility.Visible;
            this.LibraryView.Visibility = Visibility.Collapsed;
            this.Player = new PlayListPlayer(this.Current);
            this.currentPlaylist.ItemsSource = this.Player.Items;
            this.LibraryPlaylists.ItemsSource = this.MyLibrary.Playlists;
            this.LibraryMedias.ItemsSource = this.MyLibrary.Medias;
        }

        private void LibraryPlaylists_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Console.WriteLine("LibraryPlaylists_MouseDoubleClick");
            if (this.LibraryPlaylists.SelectedItem == null)
                return;
            PlayList item = (PlayList)((System.Windows.Controls.DataGrid)sender).CurrentItem;
            if (item == null)
                return;
            this.Current = item;
            this.Player = new PlayListPlayer(this.Current);
            this.currentPlaylist.ItemsSource = this.Player.Items;
            this.currentPlaylist.Items.Refresh();
        }

        private void LibraryMedias_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Console.WriteLine("LibraryMedias_MouseDoubleClick");
            if (this.LibraryMedias.SelectedItem == null)
                return;
            Media item = (Media)((System.Windows.Controls.DataGrid)sender).CurrentItem;
            if (item == null)
                return;
            this.Player.AddPlayListItem(item);
            this.currentPlaylist.Items.Refresh();
            this.Player.Current = item;
            var mw = (MyWindowsMediaPlayerV2.MainWindow)Application.Current.MainWindow;
            mw.playMedia(item);
        }

        private void btnLibPlaylist_Click(object sender, RoutedEventArgs e)
        {
            currentPlaylist.Visibility = Visibility.Visible;
            LibraryView.Visibility = Visibility.Collapsed;
        }

        private void btnLibrary_Click(object sender, RoutedEventArgs e)
        {
            currentPlaylist.Visibility = Visibility.Collapsed;
            LibraryView.Visibility = Visibility.Visible;
        }

        private void currentPlaylist_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (this.currentPlaylist.SelectedItem == null)
                return;
            Media item = (Media)((DataGrid)sender).CurrentItem;
            if (item == null)
                return;
            Console.WriteLine("play");
            var mw = (MainWindow)Application.Current.MainWindow;
            mw.playMedia(item);
        }

        private void currentPlaylist_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                DataGrid grid = (DataGrid)sender;
                if (grid.CurrentItem != null)
                {
                    Console.WriteLine("Remove media");
                    if ((Media)grid.CurrentItem == this.Player.Current)
                    {
                        var mw = (MainWindow)Application.Current.MainWindow;
                        mw.myMediaElement.Close();
                    }
                    this.Player.RemoveMedia((Media)grid.CurrentItem);
                    currentPlaylist.Items.Refresh();
                }
            }
        }
        private void LibraryMedias_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                DataGrid grid = (DataGrid)sender;
                if (grid.CurrentItem != null)
                {
                    Console.WriteLine("Remove media from library");
                    this.MyLibrary.RemoveMedia((Media)grid.CurrentItem);
                    this.LibraryMedias.Items.Refresh();
                }
            }
        }
        private void LibraryPlayLists_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                DataGrid grid = (DataGrid)sender;
                if (grid.CurrentItem != null)
                {
                    Console.WriteLine("Remove media from library");
                    this.MyLibrary.RemovePlayList((PlayList)grid.CurrentItem);
                    
                    this.LibraryPlaylists.Items.Refresh();
                }
            }
        }
    }
}

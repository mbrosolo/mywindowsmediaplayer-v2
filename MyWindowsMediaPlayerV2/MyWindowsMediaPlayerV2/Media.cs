﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Shell32;
using System.IO;
using System.Windows.Forms;

namespace MyWindowsMediaPlayerV2
{
    public class Media
    {
        public string Path { get; set; }
        public string Filename { get; set; }
        public string Size { get { return _metadatas.ContainsKey("Size") ? _metadatas["Size"] : "N/A"; } }
        public string Type { get { return _metadatas.ContainsKey("Type") ? _metadatas["Type"] : ""; } }
        public string Artist { get { return _metadatas.ContainsKey("Artist") ? _metadatas["Artist"] : ""; } }
        public string Album { get { return _metadatas.ContainsKey("Album") ? _metadatas["Album"] : ""; } }
        public string Author { get { return _metadatas.ContainsKey("Author") ? _metadatas["Author"] : ""; } }
        public string Title
        {
            get
            {
                if (_metadatas.ContainsKey("Title") && _metadatas["Title"] != "")
                    return _metadatas["Title"];
                else if (_metadatas.ContainsKey("Filename") && _metadatas["Filename"] != "")
                    return _metadatas["Filename"];
                return Path;
            }
        }
        public string Duration { get { return _metadatas.ContainsKey("Duration") ? _metadatas["Duration"] : ""; } }
        public string TotalThroughput { get { return _metadatas.ContainsKey("TotalThroughput") ? _metadatas["TotalThroughput"] : ""; } }

        private Dictionary<string, string> _metadatas;
        public Dictionary<string, string> MetaData { get; set; }

        public Media(string path)
        {
            this._metadatas = new Dictionary<string, string>();
            this.Path = path;
            GetAllMetaData();
        }

        public void SetMetaData(string key, string value)
        {
            this._metadatas[key] = value;
        }

        public string GetMetaData(string key)
        {
            return this._metadatas[key];
        }

        private void GetAllMetaData()
        {
            Shell shell = new Shell32.Shell();
            Shell32.Folder folder = shell.NameSpace(System.IO.Path.GetDirectoryName(Path));
            Shell32.FolderItem folderItem = folder.ParseName(System.IO.Path.GetFileName(Path));

            string filename = Path.Substring(this.Path.LastIndexOf("\\") + 1);
            Shell32.FolderItem tmp = folder.ParseName(filename);
            if (tmp != null)
            {
                this._metadatas.Add("Filename", folder.GetDetailsOf(tmp, 0));
                this._metadatas.Add("Size", folder.GetDetailsOf(tmp, 1));
                this._metadatas.Add("Type", folder.GetDetailsOf(tmp, 2));
                this._metadatas.Add("Artist", folder.GetDetailsOf(tmp, 13));
                this._metadatas.Add("Album", folder.GetDetailsOf(tmp, 14));
                //this._metadatas.Add("Author", folder.GetDetailsOf(tmp, 20));
                this._metadatas.Add("Title", folder.GetDetailsOf(tmp, 21));
                this._metadatas.Add("Duration", folder.GetDetailsOf(tmp, 27));
                //this._metadatas.Add("TotalThroughput", folder.GetDetailsOf(tmp, 286));
            }
            else
            {
                this._metadatas.Add("Filename", "");
                this._metadatas.Add("Size", "");
                this._metadatas.Add("Type", "");
                this._metadatas.Add("Artist", "");
                this._metadatas.Add("Album", "");
                //this._metadatas.Add("Author", "");
                this._metadatas.Add("Title", "");
                this._metadatas.Add("Duration", "");
                //this._metadatas.Add("Total throwput", "");
            }
        }

    }
}

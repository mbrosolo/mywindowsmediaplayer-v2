﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyWindowsMediaPlayerV2
{
    public class PlayListPlayer : PlayList
    {
        public bool Done
        {
            get;
            private set;
        }

        public bool Random
        {
            get;
            private set;
        }
        private int _currentIndex;

        public Media Current
        {
            get {
                if (this._items.Count == 0 || this.Done) {
                    return null;
                } else {
                    return this._items[this._currentIndex];
                }
            }
            set {
                int idx = this._items.IndexOf(value);
                if (idx < 0) {
                    idx = 0;
                }
                this._currentIndex = idx;
            }
        }

        private PlayList _readplaylist;

        public PlayListPlayer(PlayList  pl) : base("") {
            this._items = new List<Media>(pl.Items);
            this._currentIndex = 0;
            this.Done = false;
            this.Random = false;
            this._readplaylist = pl;
        }

        public void Shuffle()
        {
            PlayListPlayer.Shuffle<Media>(this._items);
            this.Random = true;
        }

        public void NextMedia() { 
            if (this._currentIndex + 1 == this._items.Count)
            {
                this.Done = true;
            }
            else
            {
                this._currentIndex++;
            }
        }

        public void Next() {
            this.Done = false;
            if (this._currentIndex + 1 == this._items.Count)
            {
                this._currentIndex = 0;
            }
            else
            {
                this._currentIndex++;
            }

        }

        public void Prev() {
            if (this._currentIndex == 0) {
                this._currentIndex = this._items.Count - 1;
            } else {
                this._currentIndex--;
            }
        }

        public static void Shuffle<T>(IList<T> list)
        {
            Random rng = new Random();
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public void RemoveMedia(Media m)
        {
            if (!this._items.Contains(m)) {
                return;
            }

            if (this.Current == m)
            {
                this.Next();
            }
            Media oldCurrent = this.Current;
            this.Items.Remove(m);
            this._currentIndex = this._items.IndexOf(oldCurrent);
        }

        public void Save()
        {
            this._readplaylist.Items = new List<Media>(this._items);
        }
        public void Save(string filename)
        {
            this._readplaylist.Items = new List<Media>(this._items);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.XPath;
using System.Xml;

namespace MyWindowsMediaPlayerV2
{
    public class PlayList
    {
        public string FileName { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }

        public List<Media> Items { 
            get {
                return this._items;
            }
        }

        private List<Media> _items;

        public PlayList(string filename) {
            this._items = new List<Media>();
            this.FileName = filename;

            try {
                XPathDocument doc = new XPathDocument(filename);
                XPathNavigator nav = doc.CreateNavigator();

                XPathNavigator header = nav.SelectSingleNode("Root/Header");
                this.Title = header.SelectSingleNode("title").Value;
                this.Author = header.SelectSingleNode("author").Value;

                XPathNodeIterator iter = nav.Select("Root/Files/item");

                foreach (XPathNavigator item in iter)
                {

                    Console.WriteLine("tamer = " + item.Value);
                    string cfilename = item.SelectSingleNode("filename").Value;
                    Media newitem = new Media(cfilename);
                    this._items.Add(newitem);

                }
            }x catch (Exception e) {
                Console.WriteLine(e.Message);
            }
        }

        public void     AddPlayListItem(Media item) {
            this._items.Add(item);
        }

        public void     AppendPlayList(PlayList p1) {
        }

        public void Serialize()
        {
            this.Serialize(this.FileName);
        }

        public void Serialize(string filename) {
            XmlDocument doc = new XmlDocument();

            XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", null, null);
            doc.AppendChild(dec);

            doc.AppendChild(doc.CreateElement("Root"));

            XmlElement header = doc.CreateElement("Header");
            doc.DocumentElement.AppendChild(header);
            XmlElement tmp = null;

            tmp = doc.CreateElement("title");
            tmp.InnerText = this.Title;
            header.AppendChild(tmp);

            tmp = doc.CreateElement("author");
            tmp.InnerText = this.Author;
            header.AppendChild(tmp);

            XmlElement files = doc.CreateElement("Files");
            doc.DocumentElement.AppendChild(files);

            foreach (Media item in this._items) {
                XmlElement tmpnode = doc.CreateElement("item");
                tmp = doc.CreateElement("filename");
                tmp.InnerText = item.Path;
                tmpnode.AppendChild(tmp);
                files.AppendChild(tmpnode);
            }

            doc.Save(filename);
        }

        public void Shuffle() {
            PlayList.Shuffle<Media>(this._items);
        }

        public static void Shuffle<T>(IList<T> list)
        {
            Random rng = new Random();
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

    }
}

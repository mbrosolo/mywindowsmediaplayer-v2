﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.XPath;
using System.Xml;

namespace MyWindowsMediaPlayerV2
{
    public class PlayList
    {
        public string FileName { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public bool Good { get; set; }

        public List<Media> Items
        {
            get
            {
                return this._items;
            }
            set 
            {
                this._items = value;
            }
        }

        protected List<Media> _items;

        public PlayList(string filename) {
            this._items = new List<Media>();
            this.FileName = filename;
            this.Good = true;
            this.Title = "Default";
            try {
                XPathDocument doc = new XPathDocument(filename);
                XPathNavigator nav = doc.CreateNavigator();

                XPathNavigator header = nav.SelectSingleNode("Root/Header");
                this.Title = header.SelectSingleNode("title").Value;
                this.Author = header.SelectSingleNode("author").Value;

                XPathNodeIterator iter = nav.Select("Root/Files/item");

                foreach (XPathNavigator item in iter)
                {
                    string cfilename = item.SelectSingleNode("filename").Value;
                    Media newitem = new Media(cfilename);
                    this._items.Add(newitem);

                }
            } catch (Exception e) {
                Console.WriteLine(e.Message);
                this.Good = false;
            }
        }

        public PlayList(PlayList other) {
            this._items = new List<Media>(other.Items);
            this.FileName = other.FileName;
            this.Title = other.Title;
            this.Author = other.Author;
            this.Good = true;
            this.Title = "Default";
        }

        public void     AddPlayListItem(Media item) {
            this._items.Add(item);
        }

        public void     AppendPlayList(PlayList p1) {
        }

        public void Serialize()
        {
            this.Serialize(this.FileName);
        }

        public void Serialize(string filename) {
            XmlDocument doc = new XmlDocument();

            XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", null, null);
            doc.AppendChild(dec);

            doc.AppendChild(doc.CreateElement("Root"));

            XmlElement header = doc.CreateElement("Header");
            doc.DocumentElement.AppendChild(header);
            XmlElement tmp = null;

            tmp = doc.CreateElement("title");
            tmp.InnerText = this.Title;
            header.AppendChild(tmp);

            tmp = doc.CreateElement("author");
            tmp.InnerText = this.Author;
            header.AppendChild(tmp);

            XmlElement files = doc.CreateElement("Files");
            doc.DocumentElement.AppendChild(files);

            foreach (Media item in this._items) {
                XmlElement tmpnode = doc.CreateElement("item");
                tmp = doc.CreateElement("filename");
                tmp.InnerText = item.Path;
                tmpnode.AppendChild(tmp);
                files.AppendChild(tmpnode);
            }

            System.IO.File.WriteAllText(filename, string.Empty);
            doc.Save(filename);
        }

    }
}
